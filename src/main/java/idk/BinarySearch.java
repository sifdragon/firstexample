package idk;

public class BinarySearch {
    private double array;

    public BinarySearch (double [] array){
        this.array=array;
    }

    public int search(double toCompare)
    {
        if (array == null){ return -1;}
        int l = -1;
        int r = array.length;
        int m;
        int index = -1;
        while (l<=r){
            m = (l+r)/2;
            if (toCompare > array(m)){ l = m +1; } else if  (toCompare < array(m)) { r = m-1; } else { index = m; break; }
        }
        //System.out.println(this.array);
        return index;
    }

    public static void main(String[] args) {
        BinarySearch bs1=new BinarySearch(new double[]{0,1});
        System.out.println("Search for -2 :" + bs1.search(-2));
        System.out.println("Search for 0 :" + bs1.search(0));
        System.out.println("Search for 1 :" + bs1.search(1));

        BinarySearch bs2=new BinarySearch(new double[]{-1,1,2,3,45,67});
        System.out.println("Search for -3 :" + bs2.search(-3));
        System.out.println("Search for -1 :" + bs2.search(-1));
        System.out.println("Search for 2 :" + bs2.search(2));
        System.out.println("Search for 3 :" + bs2.search(3));
        System.out.println("Search for 67 :" + bs2.search(67));

        BinarySearch bs3=new BinarySearch(new double[]{-1,1,2,3,34,45,67});
        System.out.println("Search for -3 :" + bs3.search(-3));
        System.out.println("Search for 3 :" + bs3.search(3));
        System.out.println("Search for 67 :" + bs3.search(67));
        System.out.println("Search for 1 :" + bs3.search(1));
        System.out.println("Search for -1 :" + bs3.search(-1));

        BinarySearch bs4=new BinarySearch(new double[]{0});
        System.out.println("Search for -2 :" + bs4.search(-2));
        System.out.println("Search for 0 :" + bs4.search(0));
        System.out.println("Search for 1 :" + bs4.search(1));

        BinarySearch bs5=new BinarySearch(new double[]{});
        System.out.println("Search for -2 :" + bs5.search(-2));
        System.out.println("Search for 0 :" + bs5.search(0));
        System.out.println("Search for 1 :" + bs5.search(1));

        BinarySearch bs6=new BinarySearch(null);
        System.out.println("Search for -2 :" + bs6.search(-2));
        System.out.println("Search for 0 :" + bs6.search(0));
        System.out.println("Search for 1 :" + bs6.search(1));

    }

}


