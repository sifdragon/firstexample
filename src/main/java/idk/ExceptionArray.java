package idk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ExceptionArray {

    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Введите число элементов: ");
        int numberOfElements = Integer.parseInt(br.readLine());
        String[] strings = new String[numberOfElements];
        for (int i=0; i<strings.length; i++) {
            strings[i] = br.readLine();
        }

        System.out.print("Введите элемент, к которому надо обратитbся: ");
        int index = Integer.parseInt(br.readLine());

        try {
            System.out.println(strings[index]);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Ошибка: обрпащение " + index + " элементу массива из " + strings.length +" элементов");
        }
        finally {
            br.close();
        }
    }

}
