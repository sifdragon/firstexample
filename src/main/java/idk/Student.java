package itis.oop;

import java.util.Date;

/**
 * Created by Ilya Evlampiev on 03.10.2018.
 */
public class Student extends Object {
    private int year;
    String firstName;
    String secondName;
    String patronymic;
    Date dateOfBirth;
    String email;
    //String bitbucketRepo;


    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    Student(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    int age() {
        Date now = new Date();
        int yearNow = now.getYear();
        int yearOfBirth = this.dateOfBirth.getYear();
        return yearNow - yearOfBirth;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        } else if (object instanceof Student) {
            if (this.getDateOfBirth().equals(((Student) object).getDateOfBirth()) &&
                    this.getFirstName().equals(((Student) object).getFirstName()) &&
                    this.getSecondName().equals(((Student) object).getSecondName()) &&
                    this.getPatronymic().equals(((Student) object).getPatronymic())) {
                return true;

            }
        } else {
            return false;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = year;
        result = 31 * result + firstName.hashCode();
        result = 31 * result + secondName.hashCode();
        result = 31 * result + patronymic.hashCode();
        result = 31 * result + dateOfBirth.hashCode();
        if (email!=null ) {result = 31 * result + email.hashCode();};
        return result;
    }

    @Override
    public String toString() {
        return "Student {" + "firstName=" + firstName  + ", SecondName='" + SecondName + ",age=" + age + ",patronymic='" + patronymic + ",email=" + email + ",date =" + date + "}";

    }

}
