package itis;
import java.util.Scanner;
public class Task013 {
    public static void main(String[] args) {
        System.out.println("Введите целое число");
        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();
        double sum = 1;
        for (double i = 1; i <= n; i++) {
            sum *= ((2*i)/((2*i)-1))*((2*i)/((2*i)+1));
        }
        System.out.println(sum);
    }
}