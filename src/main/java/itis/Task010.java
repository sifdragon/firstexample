package itis;
import java.util.Scanner;
public class Task010 {
    public static void main(String [] args){
        System.out.println("Введите целое число");
        Scanner scan = new Scanner(System.in);
        int x = scan.nextInt();
        int y = x > 2 ? ((x*x)-1)/(x+2):
                ((x > 0) && (x <= 2)) ? (((x * x) - 1) * (x + 2)) :
                        (x * x) * (((x * x) - 1)) / (x + 2) * (1+2*x);
        System.out.println(y);
    }

}
