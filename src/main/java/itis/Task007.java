package itis;

public class Task007 {
    public static void main(String[] args){
      float a = 15;
      float b = 10;
      System.out.println("a + b = " + (a+b));
      System.out.println("a - b = " + (a-b));
      System.out.println("b - a = " + (b-a));
      System.out.println("a * b = " + (a*b));
      System.out.println("a / b = " + (a/b));
      System.out.println("a % b = " + (a%b));
      System.out.println("b / a = " + (b/a));
      System.out.println("b % a = " + (b%a));
    }
}
