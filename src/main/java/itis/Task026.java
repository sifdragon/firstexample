package itis;

import java.util.Scanner;
public class Task026 {
    public static void main(String[] srgs) {
        Scanner sc = new Scanner(System.in);

        final double EPS = 0.000000001;
        double sl = 1000000000;
        double prsl = 0;
        double sum = 0;
        int i = 1;
        int fac = 1;
        int k = 2;

        System.out.println("Ввод x");
        double x = sc.nextDouble();

        while ((Math.abs(sl - prsl)) > EPS) {
            prsl = (Math.pow(x - 1, i)) / ((Math.pow(3, i) * (i * i + 3) * fac));
            fac = fac * k;
            sl = (Math.pow(x - 1, i + 1)) / ((Math.pow(3, i + 1) * ((i + 1) * (i + 1) + 3) * fac));
            k += 1;
            i += 2;
            fac = fac * k;
            k += 1;

            sum = sum + prsl + sl;
        }
        System.out.println(sum);
    }
    }