package itis;

import java.util.Scanner;
public class Task019 {
    public static void main (String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите n");
        int n = sc.nextInt();
        int del = 0;

        for (int j = 2; j <= n; j++) {
            for (int i = 1; i <= Math.floor(Math.sqrt(j)); i++) {
                if (j % i == 0) {
                    del = del + i + j / i;
                }
            }
            if ( (n < 1000000) && (del - j == j)) {
                System.out.println(j);
                del = 0;
            }
            else {del = 0;}
        }
    }
}