package itis;

import java.util.Scanner;
public class Task020 {
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Введите количество строк");
        int n = sc.nextInt();

        for (int i = 1; i <=n; i++) {
            for (int j = n - i; j >= 0; j--) {
                System.out.print("*");
            }

            for (int k = 1; k <= 2 * i - 1; k++) {
                System.out.print(0);
            }
            for (int j = n - i; j >= 0; j--) {
                System.out.print("*");
            }
            System.out.println();

        }

        for (int j = 1; j <= 2 * n + 1; j++) {
            System.out.print(0);
        }
        System.out.println();

        for (int i = 1; i <= n; i++) {
            for (int k = 1; k <= i; k++) {
                System.out.print("*");
            }

            for (int j = 2 * n - 2 * i; j >= 0; j--) {
                System.out.print(0);
            }

            for (int k = 1; k <= i; k++) {
                System.out.print("*");
            }
            System.out.println();

        }
    }
}
