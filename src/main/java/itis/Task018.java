package itis;
import java.util.Scanner;
public class Task018 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ввод n");
        int n = sc.nextInt();

        System.out.println("Ввод x");
        double x = sc.nextDouble();

        double sum = 0;

        for (int i = 0; i <= n; i++) {
            System.out.println("Введите очередное число ");
            double a = sc.nextDouble();

            a *= Math.pow(x, i);
            sum += a;
        }
        System.out.println(sum);

    }
}