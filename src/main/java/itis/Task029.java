package itis;

import java.util.Scanner;
public class Task029 {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);

        System.out.println("Ввод системы счисления");
        int k = sc.nextInt();

        System.out.println("Ввод числа");
        int x = sc.nextInt();

        double newx = 0;
        int i = 0;

        while (x > 0){
            int ch = x % 10;
            newx = newx + ch * Math.pow(k, i);
            i+=1;
            x = x / 10;
        }
        System.out.println(newx);
    }
}