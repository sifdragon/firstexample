package itis;

import java.util.Scanner;
public class Task024 {
    public static void main(String[] srgs) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Ввод x ");
        double x = sc.nextDouble();

        final double EPS = 0.000000001;
        double sl = 1000000000;
        double prsl = 0;
        double sum = 0;
        int i = 1;

        while ((Math.abs(sl - prsl)) > EPS){
            prsl = 1 / (i * Math.pow(9, i) * Math.pow(x - 1, 2 * i));
            sl = 1 / (i * Math.pow(9, i) * Math.pow(x - 1, 2 * i));
            i+=2;
            sum = sum + prsl + sl;
        }
        System.out.println(sum);
    }
}