package itis;

public class Task023 {
    public static void main(String[] srgs) {
        final double EPS = 0.000000001;
        double sl = 1000000000;
        double prsl = 0;
        double sum = 0;
        int i = 1;

        while ((Math.abs(sl - prsl)) > EPS){
            prsl = (2 * i + 3) / (5 * Math.pow(i, 4) + 1);
            sl = (2 * (i + 1) + 3) / (5 * Math.pow(i + 1, 4) + 1);
            i+=2;
            sum = sum + prsl + sl;
        }
        System.out.println(sum);
    }
}