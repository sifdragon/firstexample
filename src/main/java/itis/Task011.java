package itis;
import java.util.Scanner;
public class Task011 {
    public static void main(String [] args) {
        System.out.println("Введите целое число");
        Scanner scan = new Scanner(System.in);
        double n = scan.nextInt();
        double even = 1;
        double odd = 1;
        for (double i = n; i > 1; i--) {
            if ((i % 2)==0) {
                even *= i;
            } else{
                odd *= i;
            }
        }
        if ((n%2)==0){
            n = even;
        } else{
            n = odd;
        }
        System.out.print(n);
        }
    }