package itis;

public class Task025 {
    public static void main(String[] srgs) {
        final double EPS = 0.000000001;
        double sl = 1000000000;
        double prsl = 0;
        double sum = 0;
        int i = 1;

        while ((Math.abs(sl - prsl)) > EPS){
            prsl = Math.pow(-1, i + 1) / (i * i + 3 * i);
            sl = Math.pow(-1, i + 2) / ((i + 1) * (i + 1) + 3 * (i + 1));
            i+=2;
            sum = sum + prsl + sl;
        }
        System.out.println(sum);
    }
}